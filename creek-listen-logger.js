/*
---------------------
Creek Listen Logger
---------------------

See readme.md for instructions.

----------------------------------------

TODO:

- Write the stuff for audio files: media_position, media_id

- Write one last log entry if they close the browser or exit the page.

- Fix the iframe-based persistence so that external links open in _parent, not in iframe.

- Could use a random interval between 30-60 seconds so that every log entry duration is not locked into 30s increments, which looks suspicious. (Though this is sort of lying anyway.) The only alternative is to rely on the browser closing or page changing, but we really can't do that.

----------------------------------------

*/

//Safely initialize objects, even if they are already defined.
if(typeof Creek == "undefined"){
  var Creek = {};
}
if(typeof Creek.settings == "undefined"){
  Creek.settings = {};
}
if(typeof Creek.settings.debug == "undefined"){
  Creek.settings.debug = {
    on: false
  };
}
if(typeof Creek.debug == "undefined"){
  Creek.debug = {};
}

//Add prefix for the debug function below
Creek.settings.debug.prefix = "Creek: ";

//Debug: Set up a cool debug function, wow.
Creek.debug.log = function(message, prefix){

  var prefix = prefix || Creek.settings.debug.prefix;

  if(Creek.settings.debug.on){

    //Split log into two lines if the debug output is an object/array
    // so that it's displayed nicely in the browser console.
    if(typeof message == "object" || typeof message == "array"){
      console.log(prefix);
      console.log(message);
    }
    //Otherwise just print the string/number/whatever.
    else{
      console.log(prefix+message);
    }

  }

}

/*

Listen Logger Constructor

*/
Creek.ListenLogger = function(init_settings){

  // -------------------------------
  //
  // Private objects and methods:
  //
  // Using "var" for objects that don't need to be accessed outside of constructor.
  //

  //Settings for the logger
  var settings = {
    intervalLengths: {
      logUpdate: init_settings.logUpdateInterval || 60000,
      durationIncrease: 1000
    },
  };

  //Update global debug on/off
  Creek.settings.debug.on = init_settings.debug || false;

  //Data sent to the remote logger API
  var log_session_data = {
    unique_id: null,
    start: new Date(),
    duration: 0,
    station_unique_id: init_settings.station_unique_id,
    stream_id: init_settings.stream_id
  }

  var intervals = {};

  var debug = function(message, prefix){
    Creek.debug.log(message, prefix);
  }

  var durationIncrease = function(){
    log_session_data.duration++; // Add 1 second.
  }

  //No need to access this publicly, since you should only use start and stop.
  var log = function(){

    $.post("http://dev-demo.creek.fm:7051/log/live", log_session_data, function(response){

      //Store unique_id for this session, so that we can continue to update the duration.
      log_session_data.unique_id = response.data.unique_id;

      Creek.debug.log(log_session_data);

    }, "json");

  }

  // -------------------------------
  //
  // Public methods:
  //
  // Using "this" for methods that should be called outside of constructor.
  //

  this.start = function(){

    Creek.debug.log("Listening session started.");

    intervals.logUpdate = setInterval(log, settings.intervalLengths.logUpdate);
    intervals.durationIncrease = setInterval(durationIncrease, settings.intervalLengths.durationIncrease);

  }

  this.stop = function(){

    Creek.debug.log("Listening session stopped.")

    //Reset the unique_id so that a new session is created in DB.
    log_session_data.unique_id = null;

    //Reset the duration.
    log_session_data.duration = 0;

    clearInterval(intervals.logUpdate);
    clearInterval(intervals.durationIncrease);

  }

}
