# Listen Logger

A simple way to log the listeners of your live streams and audio files.

Create an account at [Creek](http://creek.fm) to use this thing!

## Getting started

Use it like this:

```
var ListenLogger = new Creek.ListenLogger({
  station_unique_id: "stationname",
  stream_id: 1,
  debug: true // (Optional.)
  logUpdateInterval: 30000 // (In milliseconds. Optional.)
});

//When the stream starts, just do:
ListenLogger.start();

//...and when the stream stops:
ListenLogger.stop();
```

## Multiple streams or stations

If you have multiple stations or streams to log on the same page, then initialize each one:

```
var ListenLogger_Stream1 = new Creek.ListenLogger({
  station_unique_id: "stationname",
  stream_id: 1
});
var ListenLogger_Stream1 = new Creek.ListenLogger({
  station_unique_id: "stationname",
  stream_id: 1
});
```

Then, inside the play/stop functions:

```
function whatever_the_play_function_is_for_stream1(){
  ListenLogger_Stream1.start();
  ListenLogger_Stream1.stop();
}
```

...and:

```
function whatever_the_play_function_is_for_stream2(){
  ListenLogger_Stream2.start();
  ListenLogger_Stream2.stop();
}
```
